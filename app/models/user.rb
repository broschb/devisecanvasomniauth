class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => [:Canvas]


  def self.from_omniauth(auth)
    where(email: "#{auth.info.name}@test.com").first_or_create do |user|
    user.password = Devise.friendly_token[0,20]
  end
end
end
