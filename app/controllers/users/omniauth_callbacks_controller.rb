class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def Canvas
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      sign_in(:user, @user)
      # sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      # set_flash_message(:notice, :success, :kind => "Canvas") if is_navigational_format?
      render :inline => "all good"
    else
      session["devise.canvas_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end
end
