require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Canvas < OmniAuth::Strategies::OAuth2
      option :name, :Canvas

      option :client_options, {
          :authorize_url => "http://bdev.com:3000/login/oauth2/auth",
          :token_url => "http://bdev.com:3000/login/oauth2/token",
          :site => "http://bdev.com:3000"
        }

      uid do
        puts "UID #{access_token.params}"
        puts access_token.params['user']['id']
        access_token.params['user']['id']
      end

      info do
        {name: access_token.params['user']['name']}
      end

      # def request_phase
      #   puts 'calling request'
      #   super
      # end
      #
      # def callback_phase
      #   puts 'calling callback'
      #   super
      # end

    end
  end
end
